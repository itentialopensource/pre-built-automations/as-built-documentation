<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 04-30-2024 and will be end of life on 04-30-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Workflow Utilities](https://gitlab.com/itentialopensource/pre-built-automations/iap-workflow-utilities)

<!-- Update the below line with your artifact name -->
# As Built Documentation

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)

## Overview

<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<!-- <!--  -->
The **As Built Documentation** pre-built takes an Admin Essentials installed Pre-Built and creates a document in markdown format that includes each component of the pre-built, as well as the relationships between the components. Optionally, a full As Built template will be provided that can easily be filled in with the appropriate data. 

<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2021.1.x`
* App-Artifacts
  * `6.0.1-2021.1.0`

## Requirements

This Pre-Built requires the following:

<!-- Unordered list highlighting the requirements of the artifact -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
* Pre-Built installed in Admin Essentials

## Features

The main benefits and features of the artifact are outlined below.

<!-- Unordered list highlighting the most exciting features of the artifact -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->
* Automatically fills As-Built document with a Pre-Builts components and their relationship
* Renders document in easily transferable markdown format
* User can easily fill in any remaining information using the optional provided template


<!-- ## Future Enhancements -->

<!-- OPTIONAL - Mention if the artifact will be enhanced with additional features on the road map -->
<!-- Ex.: This artifact would support Cisco XR and F5 devices -->

## How to Install

To install this pre-built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section. 
* The artifact can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the artifact:

<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->

1. In Automation Catalog, find the **As Built Documentation** entry.
2. In the form, choose the Pre-Built you wish to create an As Built Document for and select provide full template if desired.
3. Run the automation and work any manual tasks in the workflow. 
4. The final manual task  will provide the As-Built markdown document that can be copied and pasted to the user's final destination.
